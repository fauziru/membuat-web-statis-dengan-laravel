<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Tugas CRUD
Route::get('pertanyaan', 'PertanyaanController@index')->name('pertanyaan.index');
Route::get('pertanyaan/{id}', 'PertanyaanController@show')->name('pertanyaan.show');
Route::post('pertanyaan', 'PertanyaanController@store')->name('pertanyaan.store');
Route::delete('pertanyaan/{id}', 'PertanyaanController@destroy')->name('pertanyaan.destroy');
Route::get('pertanyaan/{id}/edit', 'PertanyaanController@edit')->name('pertanyaan.edit');
Route::put('pertanyaan/{id}', 'PertanyaanController@update')->name('pertanyaan.update');
//Tugas Hari 3
Route::get('/', 'HomeController@index')->name('admin.index');
Route::get('/data-tables', 'HomeController@dataTables')->name('admin.data-tables');


// Tugas Hari 2
Route::get('/home', function ($id) {
    return view('home');
});
Route::get('/register','RegisterController@registerpage')->name('register.show');
Route::post('/welcome', 'RegisterController@daftar')->name('register.daftar');