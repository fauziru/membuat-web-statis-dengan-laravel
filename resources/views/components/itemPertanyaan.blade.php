@foreach ($pertanyaan as $key => $item)
    <div>
        <i class="fas fa-comments bg-yellow"></i>
        <div class="timeline-item">
            <span class="time"><i class="fas fa-clock"></i> 27 mins ago</span>
            <h3 class="timeline-header"><a href="#">Jay White</a> commented on your post</h3>
            <div class="timeline-body">{{ $item->isi }}</div>
            <div class="timeline-footer d-flex justify-content-end">
                <a href="{{ route('pertanyaan.edit', $item->id)}}" class="btn btn-warning btn-sm ml-1">Edit</a>
                <a href="{{ route('pertanyaan.show', $item->id)}}" class="btn btn-info btn-sm ml-1">Detail</a>
                <form action="{{ route('pertanyaan.destroy', $item->id)}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <input type="submit" class="btn btn-danger btn-sm ml-1" value="Delete">
                </form>
            </div>
        </div>
    </div> 
@endforeach