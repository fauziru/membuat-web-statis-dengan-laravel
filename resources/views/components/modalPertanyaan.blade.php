<div class="modal fade" id="modal-pertanyaan" style="display: none;" aria-modal="true">
    <!-- modal-dialog -->
    <div class="modal-dialog modal-lg">
      <!-- modal-content -->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Tanya</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="quickFormTanya" method="POST" action="#">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                      <input type="hidden" name="profile_id"  value="{{1}}" />
                    </div>
                </div>
                <div class="card-body">
                  <div class="form-group">
                    <label for="isi">Pertanyaan:</label>
                    <textarea class="form-control" name="isi" id="isi" rows="3" placeholder="Silahkan Masukan Pertanyaan ..."></textarea>
                  </div>
                </div>
            </form>
        </div>
        <div class="modal-footer justify-content-end">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          @include('components.partialLoader')
          <button id="submitTanya" type="button" class="btn btn-primary">Submit</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

@push('scripts')
<script>
    $(function () {
        var Toast = Swal.mixin({
                position: 'center',
                showConfirmButton: false,
                timer: 2000
            });
        //$('.loader').hide();
        $('#submitTanya').click(function(e) {
            e.preventDefault();
            if ($('#quickFormTanya').valid()) {    
                $('.loader').show();
                $('.btn').hide();
                $.ajax({
                    type:'POST',
                    url: '/pertanyaan',
                    data: $('#quickFormTanya').serialize(),
                    success : function(){
                        Toast.fire({icon:'success',title:'Data Berhasil diSubmmit'});
                        window.setTimeout(function(){
                            location.reload();
                        }, 1700);
                    },
                    error: function(){
                        Toast.fire({icon:'danger',title:'Data gagal diSubmit!'});
                    }
                });
                console.log('tes');
            }
        });

        $('#quickFormTanya').validate({
            rules:{
                isi:{
                    required: true
                }
            },
            messages:{
                isi:{
                    required:"Pertanyaan tidak boleh kosong"
                }
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });
    });
</script>
@endpush

