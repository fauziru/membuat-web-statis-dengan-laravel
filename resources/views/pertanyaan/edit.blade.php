@extends('admin.layouts.master', ['title'=>'Forum Tanya Jawab','activePage'=>'faq','activeTree'=>'tugasCRUD'])
@section('title','Forum Tanya Jawab')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h2>Edit Pertanyaan {{$pertanyaan->id}}</h2>
        <form action="{{ route('pertanyaan.update',$pertanyaan->id)}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="title">Isi</label>
                <input type="text" class="form-control" name="isi" value="{{ $pertanyaan->isi }}" id="title" placeholder="Masukkan Title">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
</div>
@endsection