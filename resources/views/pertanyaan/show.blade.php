@extends('admin.layouts.master', ['title'=>'Forum Tanya Jawab','activePage'=>'faq','activeTree'=>'tugasCRUD'])
@section('title','Forum Tanya Jawab')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h2>Lihat Pertanyaan {{$pertanyaan->id}}</h2>
        <h4>{{$pertanyaan->isi}}</h4>
    </div>
</div>
@endsection