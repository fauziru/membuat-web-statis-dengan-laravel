@extends('admin.layouts.master', ['title'=>'Forum Tanya Jawab','activePage'=>'faq','activeTree'=>'tugasCRUD'])
@section('title','Forum Tanya Jawab')

@section('content')
@include('components.modalPertanyaan')
<div class="row">
    <div class="col-md-12">
        <div class="float-right">
            <button type="button" class="btn btn-default" style="margin-right: 25px;margin-bottom: 10px;" data-toggle="modal" data-target="#modal-pertanyaan">
              <i class="fas fa-edit"></i> Buat Pertanyaan
            </button>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
      <!-- The time line -->
      <div class="timeline">
        <!-- timeline item -->
        @if ($pertanyaan)    
          @include('components.itemPertanyaan')
        @else
          <h3 style="margin-left: 60px">Data Tidak Tersedia</h3>
        @endif
        <!-- END timeline item -->
      </div>
    </div>
    <!-- /.col -->
</div>
<div class="row">
  @if ($pertanyaan)
    <div class="col-md-1 mx-auto mb-5">
      <button id="memuat" type="button" class="btn btn-block btn-outline-secondary">Load More</button>
      @include('components.partialLoader')
    </div>
  @endif
</div>
@endsection

@push('scripts')
    <script>
        $(function(){
            $('.loader').hide();
            
            var pageNumber = 2;
            var Toast = Swal.mixin({
                position: 'center',
                showConfirmButton: true,
                timer: 3000
            });

            $('#memuat').click(function(){
              $('#memuat').hide();
              $('.loader').show();
              $.ajax({
                type : 'GET',
                url: `/pertanyaan?page=${pageNumber}`,
                success : function(data){
                  pageNumber +=1;
                  if(data.html.length == 0){
                    Toast.fire({icon:'info',title:'Semua Data Sudah Dimuat'});
                    $('.loader').hide();
                  }else{
                    $('.timeline').append(data.html);
                    $('.loader').hide();
                    $('#memuat').show();
                  }
                }
              });
            });
            
        });
    </script>
@endpush