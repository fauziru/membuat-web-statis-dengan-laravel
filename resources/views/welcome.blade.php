<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome to Our Web App - Belajar HTML</title>
</head>
<body>
    <div>
        <h1>SELAMAT DATANG! {{ "{$data['awalNama']} {$data['akhirNama']}" }}</h1>
    </div>
    <div>
        <h2>Terima kasih telah bergabung di Sanberbook. Social Media kita bersama!</h2>
    </div>
</body>
</html>