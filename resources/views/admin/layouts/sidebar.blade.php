<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="{{ asset('assets/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('assets/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Alexander Pierce</a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item {{ $activeTree == "tugasHari3" ? "menu-open" : ""}}">
            <a href="#" class="nav-link {{ $activeTree == "tugasHari3" ? "active" : ""}}">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Tugas Harian 3
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('admin.index')}}" class="nav-link {{ $activePage == "dashboard" ? "active" : ""}}">
                  <i class="fas fa-tachometer-alt nav-icon"></i>
                  <p>Dashboard</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.data-tables')}}" class="nav-link {{ $activePage == "dataTables" ? "active" : ""}}">
                  <i class="fas fa-table nav-icon"></i>
                  <p>Data Tables</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item {{ $activeTree == "tugasCRUD" ? "menu-open" : ""}}">
            <a href="#" class="nav-link {{ $activeTree == "tugasCRUD" ? "active" : ""}}">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Tugas CRUD
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('pertanyaan.index')}}" class="nav-link {{ $activePage == "faq" ? "active" : ""}}">
                  <i class="fas fa-question-circle nav-icon"></i>
                  <p>Pertanyaan</p>
                </a>
              </li>
            </ul>
          </li>

          {{-- <li class="nav-item">
            <a href="{{ route('admin.index')}}" class="nav-link {{ $activePage == "dashboard" ? "active" : ""}}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <span class="right badge badge-danger">New</span>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('admin.data-tables')}}" class="nav-link {{ $activePage == "dataTables" ? "active" : ""}}">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Data Tables
                <span class="right badge badge-danger">New</span>
              </p>
            </a>
          </li> --}}

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>