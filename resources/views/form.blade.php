<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulir Mendaftar - Belajar HTML</title>
</head>
<body>
    <h1>
        Buat Account Baru!
    </h1>
    <h3>
        Sign Up Form
    </h3>
    <form action="{{ route('register.daftar')}}" method="POST">
        {{ csrf_field() }}
        <div>
            <label for="awalNama">First Name:</label><br><br>
            <input id="awalNama" type="text" name="awalNama"><br><br>
            <label for="akhirNama">Last Name:</label><br><br>
            <input id="akhirNama" type="text" name="akhirNama">
        </div><br>
        <div>
            <label>Gender:</label><br><br>
            <label>
                <input type="radio" name="gender" value="1">
                Male
            </label><br>
            <label>
                <input type="radio" name="gender" value="2">
                Female
            </label><br>
            <label>
                <input type="radio" name="gender" value="3">
                Other
            </label>
        </div><br>
        <div>
            <label for="nationality">Nationality:</label><br><br>
            <select name="nationality" id="nationality">
                <option value="1">Indonesia</option>
                <option value="2">Malaysia</option>
                <option value="3">Thailand</option>
            </select>
        </div><br>
        <div>
            <label>Language Spoken:</label><br><br>
            <label>
                <input type="checkbox" name="nat" value="1">
                Indonesia
            </label><br>
            <label>
                <input type="checkbox" name="nat" value="2">
                English
            </label><br>
            <label>
                <input type="checkbox" name="gender" value="3">
                Other
            </label>
        </div><br>
        <div>
            <label for="bio">Bio:</label><br><br>
            <textarea type="textarea" name="bio" id="bio" rows="10" cols="30"></textarea>
        </div>
        <button type="submit">Signup</button>
    </form>
</body>
</html>