<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        return view('index-tables');
    }

    public function dataTables(){
        return view('data-tables');
    }
}
