<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use DB,Redirect,Response;

class PertanyaanController extends Controller
{
    public function index(Request $request){
        $pertanyaan = DB::table('pertanyaan')->simplePaginate(4);
        if ($request->ajax()) {
            $obj = view('components.itemPertanyaan',compact('pertanyaan'))->render();
            return response()->json(['html'=>$obj]);
        }
        return view('pertanyaan.index',compact('pertanyaan'));
    }
    
    public function store(Request $request){
        $pertanyaan = $this->validate($request,[
            'isi'=>'required'
        ]);
        $pertanyaan = DB::table('pertanyaan')->insert([
            "isi" => $request["isi"],
            "profile_id"=> $request["profile_id"]
        ]);
        return response()->json($pertanyaan, 200);
    }

    public function edit($id){
        $pertanyaan = $this->getData($id);
        return view('pertanyaan.edit', compact('pertanyaan'));
    }

    public function update($id, Request $request){
        $pertanyaan = $this->validate($request,[
            'isi' => 'required'
        ]);
        $pertanyaan = DB::table('pertanyaan')
            ->where('id', $id)
            ->update([
                'isi' => $request["isi"]
            ]);
        return redirect()->route('pertanyaan.index');
    }

    public function destroy($id){
        $pertanyaan = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect()->route('pertanyaan.index');
    }

    public function show($id){
        $pertanyaan = $this->getData($id);
        return view('pertanyaan.show',compact('pertanyaan'));
    }

    private function getData($id){
        $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        return $pertanyaan;
    }
    
}
