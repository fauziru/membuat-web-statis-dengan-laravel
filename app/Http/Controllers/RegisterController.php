<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function registerpage(){
        return view('form');
    }

    public function daftar(Request $req){
        $data = $req->all();
        return view('welcome', compact('data'));
    }
}